/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable function-paren-newline */
/* eslint-disable implicit-arrow-linebreak */
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { refreshLocation } from '../../actions';

import {
  Image,
  Container,
  ContainerInfo,
  ContainerTemp,
  Temp,
  Description,
  GridItems,
  Item,
  ItemOne,
  ItemTwo,
  Refresh,
} from './styles';

const CurrentLocation = () => {
  const data = useSelector((state) => state.list.currentLocation);
  const dispatch = useDispatch();

  return (
    <Container>
      {data && (
        <>
          <ContainerInfo>
            <Image
              src={`https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/${data.weather[0].icon}.svg`}
              alt=""
            />
            <ContainerTemp>
              <Temp>
                {data.main.temp.toFixed()}
                °C
              </Temp>
              <Description>{data.weather[0].description}</Description>
            </ContainerTemp>
          </ContainerInfo>
          <GridItems>
            <Item>
              <ItemOne>
                {data.main.temp_min.toFixed()}
                °C
              </ItemOne>
              <ItemTwo>Temp. minimum</ItemTwo>
            </Item>
            <Item>
              <ItemOne>
                {data.main.temp_max.toFixed()}
                °C
              </ItemOne>
              <ItemTwo>Temp. maximum</ItemTwo>
            </Item>
            <Item>
              <ItemOne>
                {data.main.feels_like.toFixed()}
                °C
              </ItemOne>
              <ItemTwo>Feels like</ItemTwo>
            </Item>
            <Item>
              <ItemOne>
                {data.wind.speed}
                m/s
              </ItemOne>
              <ItemTwo>Wind</ItemTwo>
            </Item>
            <Item>
              <ItemOne>{data.main.humidity}%</ItemOne>
              <ItemTwo>Humidity</ItemTwo>
            </Item>
            <Item>
              <ItemOne>{data.main.pressure}</ItemOne>
              <ItemTwo>Pressure</ItemTwo>
            </Item>
          </GridItems>
          <Refresh onClick={() => dispatch(refreshLocation())}>
            Refresh location
          </Refresh>
        </>
      )}
    </Container>
  );
};

export default CurrentLocation;
