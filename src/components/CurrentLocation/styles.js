import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;

export const ContainerInfo = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Image = styled.img`
  width: 228px;
  filter: invert(100%);
  @media (max-width: 510px) {
    width: 120px;
  }
`;

export const ContainerTemp = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-height: 125px;
`;

export const Temp = styled.p`
  margin: 0;
  font-size: 100px;
  @media (max-width: 510px) {
    font-size: 70px;
  }
`;

export const Description = styled.p`
  margin: 0;
  font-size: 20px;
`;

export const GridItems = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(2, 1fr);
  gap: 1px 1px;
  width: 100%;
  height: 100%;
`;

export const Item = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const ItemOne = styled.p`
  margin: 0;
  font-size: 25px;
  font-weight: bold;
`;

export const ItemTwo = styled.p`
  margin: 0;
  font-size: 20px;
  @media (max-width: 510px) {
    font-size: 12px;
  }
`;

export const Refresh = styled.p`
  cursor: pointer;
`;
