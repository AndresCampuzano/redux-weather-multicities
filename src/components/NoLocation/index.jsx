/* eslint-disable function-paren-newline */
/* eslint-disable implicit-arrow-linebreak */
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { currentLocation } from '../../actions';

import Container from './styles';

import addImage from '../../assets/images/add.svg';

const NoLocation = () => {
  const API_KEY = 'c982d29f34b457c7256dfdf78eff3288';
  const temperatureUnit = 'metric';

  const [coordinates, setCoordinates] = useState();
  const dispatch = useDispatch();

  const handleLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((response) =>
        setCoordinates(response),
      );
    } else {
      console.log('Geolocation is not supported by this browser.');
    }
  };

  useEffect(() => {
    fetch(
      `https://api.openweathermap.org/data/2.5/weather?lat=${
        coordinates && coordinates.coords.latitude
      }&lon=${
        coordinates && coordinates.coords.longitude
      }&appid=${API_KEY}&units=${temperatureUnit}`,
    )
      .then((response) => {
        return response.json();
      })
      .then((response) => dispatch(currentLocation(response)))
      .catch((err) => console.error(err));
  }, [coordinates, dispatch]);

  return (
    <Container>
      <button type="button" onClick={handleLocation}>
        <img src={addImage} alt="Touch to get coordinates" />
      </button>
      <p>Press to get weather of current location</p>
    </Container>
  );
};

export default NoLocation;
