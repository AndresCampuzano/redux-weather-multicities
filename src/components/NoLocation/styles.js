import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;

  button {
    border: none;
    background: transparent;
  }
`;

export default Container;
