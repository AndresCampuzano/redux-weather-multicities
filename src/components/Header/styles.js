import styled from 'styled-components';

const Container = styled.header`
  background-color: teal;
  color: ${({ theme }) => theme.white};
`;

export default Container;
