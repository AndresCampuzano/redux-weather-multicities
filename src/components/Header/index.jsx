/* eslint-disable function-paren-newline */
/* eslint-disable implicit-arrow-linebreak */
import React from 'react';
import Container from './styles';

const Header = () => {
  return (
    <Container>
      <h1>Redux Weather App 🌤</h1>
    </Container>
  );
};

export default Header;
