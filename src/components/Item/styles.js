import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 20px;
  width: 180px;
  height: 170px;
  @media (max-width: 510px) {
    width: 130px;
    height: 130px;
  }
  padding: 10px;
  background: rgba(0, 0, 0, 0.6);
  button {
    align-self: flex-end;
    border: none;
    background: transparent;
    outline: none;
  }
`;

export const CityContainer = styled.div`
  display: flex;
  align-self: end;
  padding-left: 20px;
  p {
    margin: 5px 0;
    font-size: 20px;
    @media (max-width: 510px) {
      font-size: 15px;
    }
  }
`;

export const InfoContainer = styled.div`
  display: flex;
  align-items: center;
  img {
    width: 70px;
    filter: invert(100%);
    @media (max-width: 510px) {
      width: 50px;
    }
  }
  p {
    margin: 0;
    font-size: 40px;
    @media (max-width: 510px) {
      font-size: 20px;
    }
  }
`;

export const Description = styled.div`
  align-self: end;
  padding-left: 20px;
  font-size: 20px;
  @media (max-width: 510px) {
    font-size: 15px;
  }
`;
