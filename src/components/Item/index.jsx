/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { deleteItem } from '../../actions';
import { Container, CityContainer, InfoContainer, Description } from './styles';

import trashIcon from '../../assets/images/trash.svg';

const Item = ({ city, id, apiKey, unit }) => {
  const [weather, setWeather] = useState();
  const dispatch = useDispatch();

  const handleDelete = () => {
    dispatch(deleteItem(id));
  };

  useEffect(() => {
    fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=${unit}`,
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => setWeather(data))
      .catch(() => {
        dispatch(deleteItem(id));
        alert('City not found');
        window.location.reload();
      });
  }, [city, apiKey, unit, dispatch, id]);
  // console.log('Weather: ', weather);

  return (
    <Container>
      {weather !== undefined ? (
        <>
          <CityContainer>
            <p>{weather.name},</p>
            <p>{weather.sys.country}</p>
          </CityContainer>
          <InfoContainer>
            <img
              src={`https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/${weather.weather[0].icon}.svg`}
              alt={weather.weather[0].description}
              title={weather.weather[0].description}
            />
            <p>
              {weather.main.temp.toFixed()}
              °C
            </p>
          </InfoContainer>
          <Description>{weather.weather[0].description}</Description>
          <button type="button" onClick={handleDelete}>
            <img src={trashIcon} alt="Delete city" />
          </button>
        </>
      ) : (
        'Loading city...'
      )}
    </Container>
  );
};
export default Item;
