import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addItemToList } from '../../actions';

import { Input, FormStyled } from './styles';

const Form = () => {
  // Local Hook
  const [city, setCity] = useState('');
  const updateCity = (e) => {
    setCity(e.target.value);
  };

  // Redux
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(addItemToList(city));
    setCity(''); // Cleaning input
  };

  return (
    <FormStyled onSubmit={handleSubmit}>
      <Input
        type="text"
        name="city"
        id="city"
        value={city}
        onChange={updateCity}
        placeholder="type the city you want to save..."
        required
      />
    </FormStyled>
  );
};

export default Form;
