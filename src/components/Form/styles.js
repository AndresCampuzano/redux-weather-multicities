import styled from 'styled-components';

export const FormStyled = styled.form`
  padding: 4% 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Input = styled.input`
  margin: 0 auto;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  width: 100%;
  max-width: 720px;
  height: 50px;
  border: none;
  border-radius: 20px;
  padding-left: 30px;
  background-color: #fafafa;
  outline: none;
  color: #7c7c7c;
`;
