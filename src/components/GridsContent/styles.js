import styled from 'styled-components';

export const PaddingAndMargin = styled.div`
  margin: 0 auto;
  max-width: 1400px;
  padding: 0 5%;
`;

export const GridTitles = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 20px;
  grid-row-gap: 20px;
  div {
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
  }
  text-shadow: 2px 2px 5px #727272;
  color: #fafafa;
`;

export const P = styled.p`
  margin: 0;
  font-size: 16px;
`;

export const H1 = styled.h1`
  white-space: nowrap;
  margin: 0;
  font-size: 48px;
  @media (max-width: 510px) {
    font-size: 30px;
  }
`;

export const H4 = styled.h4`
  margin: 0;
  font-size: 20px;
  @media (max-width: 1245px) {
    display: none;
  }
`;

export const GridWeather = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 20px;
  grid-row-gap: 20px;
  @media (max-width: 1245px) {
    display: flex;
    flex-direction: column;
  }
`;

export const Grid01 = styled.div`
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 20px;
  height: 380px;
  background: rgba(0, 0, 0, 0.6);
`;

export const Grid02 = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  @media (max-width: 768px) {
    grid-template-columns: repeat(2, 1fr);
  }
  gap: 20px 20px;
  /* width: 100%;
  height: 100%; */
`;
