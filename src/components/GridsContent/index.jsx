/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { useSelector } from 'react-redux';
import {
  PaddingAndMargin,
  GridTitles,
  P,
  H1,
  H4,
  GridWeather,
  Grid01,
  Grid02,
} from './styles';
import NoLocation from '../NoLocation';
import CurrentLocationCard from '../CurrentLocation';
import ItemSaved from '../Item';

const GridsContent = () => {
  const list = useSelector((state) => state.list);
  const API_KEY = 'c982d29f34b457c7256dfdf78eff3288';
  const temperatureUnit = 'metric';
  const code = useSelector((state) => state.list.currentLocation);
  // console.log('code: ', code);
  return (
    <PaddingAndMargin>
      <GridTitles>
        <div>
          <P>Current city</P>
          {code && code.cod === 200 ? (
            <H1>
              {code.name}, {code.sys.country}
            </H1>
          ) : (
            'Press down to get current location'
          )}
        </div>
        <div>
          <H4>Saved cities</H4>
        </div>
      </GridTitles>
      <GridWeather>
        <Grid01>
          {code && code.cod === 200 ? <CurrentLocationCard /> : <NoLocation />}
        </Grid01>
        <Grid02>
          {list.locations.map((info) => (
            <ItemSaved
              city={info.city}
              key={info.id}
              id={info.id}
              apiKey={API_KEY}
              unit={temperatureUnit}
            />
          ))}
        </Grid02>
      </GridWeather>
    </PaddingAndMargin>
  );
};

export default GridsContent;
