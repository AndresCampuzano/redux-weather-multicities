import React from 'react';
import HomeStyled from './styles';
// import Header from '../../components/Header';
import Form from '../../components/Form';
import GridsContent from '../../components/GridsContent';
// import ItemList from '../../components/ItemList';
// import CurrentLocation from '../../components/CurrentLocation';

// Backgrounds
import lqgLf6I from '../../assets/images/lqgLf6I.jpg';

const Home = () => {
  const customBackground = {
    backgroundImage: `url(${lqgLf6I})`,
  };
  return (
    <HomeStyled style={customBackground}>
      {/* <Header /> */}
      {/* <CurrentLocation /> */}
      <Form />
      <GridsContent />
      {/* <ItemList /> */}
    </HomeStyled>
  );
};

export default Home;
