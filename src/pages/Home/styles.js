import styled from 'styled-components';

const HomeStyled = styled.div`
  height: 100vh;
  width: 100vw;
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  @media (max-width: 1245px) {
    height: auto;
  }
`;

export default HomeStyled;
