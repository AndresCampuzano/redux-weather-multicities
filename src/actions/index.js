export const addItemToList = (city) => {
  return {
    type: 'ADD_ITEM',
    payload: city,
  };
};

export const deleteItem = (id) => {
  return {
    type: 'DELETE_ITEM',
    payload: id,
  };
};

export const currentLocation = (data) => {
  return {
    type: 'CURRENT_LOCATION',
    payload: data,
  };
};

export const refreshLocation = () => {
  return {
    type: 'REFRESH_LOCATION',
  };
};
