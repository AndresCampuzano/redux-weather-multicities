import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
    ${'' /* html {
        scroll-behavior: smooth;
    } */}
    body {
        margin: 0;
        padding: 0;
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        color: ${({ theme }) => theme.white};
    }
    a {
        color: ${({ theme }) => theme.darkBlue};
        text-decoration: none;
    }
    button {
        cursor: pointer;
    }
`;

export default GlobalStyles;
