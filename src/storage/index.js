// Redux
import { combineReducers, compose, createStore } from 'redux';
// LocalStorage
import persistState from 'redux-localstorage';
import reducer from '../reducer';

const AllReducers = combineReducers({
  list: reducer,
});
// list is used for list.map

const MainEnhancer = compose(
  persistState('list'),
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

const store = createStore(AllReducers, MainEnhancer);

export default store;
