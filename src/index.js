import React from 'react';
import ReactDOM from 'react-dom';

// Redux
import { Provider } from 'react-redux';
// Styles
import { ThemeProvider } from 'styled-components';
import Variables from './assets/styles/Variables';
import GlobalStyles from './assets/styles/GlobalStyles';

import App from './pages/Home';
import * as serviceWorker from './serviceWorker';

import store from './storage';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={Variables}>
        <GlobalStyles />
        <App />
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

serviceWorker.unregister();
