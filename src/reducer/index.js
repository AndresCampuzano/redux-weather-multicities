const initialState = {
  locations: [
    { city: 'Medellin', id: 9452051 },
    { city: 'Bogota', id: 8832038 },
  ],
  currentLocation: [],
};
const ItemsManagement = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_ITEM':
      return {
        ...state,
        locations: [
          ...state.locations,
          {
            city: action.payload,
            id: Math.floor(Math.random() * 9999999),
          },
        ],
      };
    case 'DELETE_ITEM':
      return {
        ...state,
        locations: [
          ...state.locations.filter((item) => item.id !== action.payload),
        ],
      };
    case 'CURRENT_LOCATION':
      return {
        ...state,
        currentLocation: action.payload,
      };
    case 'REFRESH_LOCATION':
      return {
        ...state,
        currentLocation: [],
      };
    default:
      return state;
  }
};

export default ItemsManagement;
